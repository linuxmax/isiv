﻿Public Class Form1
    Private Sub SociosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SociosToolStripMenuItem.Click
        My.Forms.Socios.MdiParent = Me
        My.Forms.Socios.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub PrestamosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrestamosToolStripMenuItem.Click
        My.Forms.Prestamos.MdiParent = Me
        My.Forms.Prestamos.Show()

    End Sub

    Private Sub LibrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LibrosToolStripMenuItem.Click
        My.Forms.Libros.MdiParent = Me
        My.Forms.Libros.Show()
    End Sub
End Class