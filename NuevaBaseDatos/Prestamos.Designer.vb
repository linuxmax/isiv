﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Prestamos
    Inherits Plantilla

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cod_socio = New System.Windows.Forms.TextBox()
        Me.libro = New System.Windows.Forms.TextBox()
        Me.socio = New System.Windows.Forms.TextBox()
        Me.cod_libro = New System.Windows.Forms.TextBox()
        Me.busca_socio = New System.Windows.Forms.Button()
        Me.busca_libro = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Text = "Prestamos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 100)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Socio"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 135)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Libro"
        '
        'cod_socio
        '
        Me.cod_socio.Location = New System.Drawing.Point(54, 100)
        Me.cod_socio.Name = "cod_socio"
        Me.cod_socio.Size = New System.Drawing.Size(66, 20)
        Me.cod_socio.TabIndex = 3
        '
        'libro
        '
        Me.libro.Location = New System.Drawing.Point(138, 135)
        Me.libro.Name = "libro"
        Me.libro.Size = New System.Drawing.Size(160, 20)
        Me.libro.TabIndex = 5
        '
        'socio
        '
        Me.socio.Location = New System.Drawing.Point(138, 100)
        Me.socio.Name = "socio"
        Me.socio.Size = New System.Drawing.Size(160, 20)
        Me.socio.TabIndex = 6
        '
        'cod_libro
        '
        Me.cod_libro.Location = New System.Drawing.Point(54, 132)
        Me.cod_libro.Name = "cod_libro"
        Me.cod_libro.Size = New System.Drawing.Size(66, 20)
        Me.cod_libro.TabIndex = 7
        '
        'busca_socio
        '
        Me.busca_socio.Location = New System.Drawing.Point(304, 96)
        Me.busca_socio.Name = "busca_socio"
        Me.busca_socio.Size = New System.Drawing.Size(25, 23)
        Me.busca_socio.TabIndex = 8
        Me.busca_socio.Text = "..."
        Me.busca_socio.UseVisualStyleBackColor = True
        '
        'busca_libro
        '
        Me.busca_libro.Location = New System.Drawing.Point(304, 135)
        Me.busca_libro.Name = "busca_libro"
        Me.busca_libro.Size = New System.Drawing.Size(25, 23)
        Me.busca_libro.TabIndex = 9
        Me.busca_libro.Text = "..."
        Me.busca_libro.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(254, 264)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Guardar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(161, 264)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "Cerrar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.NuevaBaseDatos.My.Resources.Resources.a862f852aa3ba570de2a19a325443703
        Me.PictureBox1.Location = New System.Drawing.Point(349, 87)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(200, 200)
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'Prestamos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 438)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.busca_libro)
        Me.Controls.Add(Me.busca_socio)
        Me.Controls.Add(Me.cod_libro)
        Me.Controls.Add(Me.socio)
        Me.Controls.Add(Me.libro)
        Me.Controls.Add(Me.cod_socio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Name = "Prestamos"
        Me.Text = "Prestamos"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.cod_socio, 0)
        Me.Controls.SetChildIndex(Me.libro, 0)
        Me.Controls.SetChildIndex(Me.socio, 0)
        Me.Controls.SetChildIndex(Me.cod_libro, 0)
        Me.Controls.SetChildIndex(Me.busca_socio, 0)
        Me.Controls.SetChildIndex(Me.busca_libro, 0)
        Me.Controls.SetChildIndex(Me.Button1, 0)
        Me.Controls.SetChildIndex(Me.Button2, 0)
        Me.Controls.SetChildIndex(Me.PictureBox1, 0)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cod_socio As TextBox
    Friend WithEvents libro As TextBox
    Friend WithEvents socio As TextBox
    Friend WithEvents cod_libro As TextBox
    Friend WithEvents busca_socio As Button
    Friend WithEvents busca_libro As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents PictureBox1 As PictureBox
End Class
