﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BusquedaLibro
    Inherits Plantilla

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.titulo_libro = New System.Windows.Forms.TextBox()
        Me.lb1 = New System.Windows.Forms.Label()
        Me.buscar_libro = New System.Windows.Forms.Button()
        Me.DataGridViewLibros = New System.Windows.Forms.DataGridView()
        Me.CodigoISBNDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TituloDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AutorDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StockDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LibrosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DBNETDataSet = New NuevaBaseDatos.DBNETDataSet()
        Me.LibrosTableAdapter = New NuevaBaseDatos.DBNETDataSetTableAdapters.LibrosTableAdapter()
        Me.cancelar = New System.Windows.Forms.Button()
        Me.seleccionar = New System.Windows.Forms.Button()
        CType(Me.DataGridViewLibros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LibrosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Text = "Buscar Libro"
        '
        'titulo_libro
        '
        Me.titulo_libro.Location = New System.Drawing.Point(49, 103)
        Me.titulo_libro.Name = "titulo_libro"
        Me.titulo_libro.Size = New System.Drawing.Size(100, 20)
        Me.titulo_libro.TabIndex = 1
        '
        'lb1
        '
        Me.lb1.AutoSize = True
        Me.lb1.Location = New System.Drawing.Point(13, 106)
        Me.lb1.Name = "lb1"
        Me.lb1.Size = New System.Drawing.Size(30, 13)
        Me.lb1.TabIndex = 2
        Me.lb1.Text = "Libro"
        '
        'buscar_libro
        '
        Me.buscar_libro.Location = New System.Drawing.Point(168, 101)
        Me.buscar_libro.Name = "buscar_libro"
        Me.buscar_libro.Size = New System.Drawing.Size(75, 23)
        Me.buscar_libro.TabIndex = 3
        Me.buscar_libro.Text = "Buscar"
        Me.buscar_libro.UseVisualStyleBackColor = True
        '
        'DataGridViewLibros
        '
        Me.DataGridViewLibros.AllowUserToAddRows = False
        Me.DataGridViewLibros.AllowUserToDeleteRows = False
        Me.DataGridViewLibros.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewLibros.AutoGenerateColumns = False
        Me.DataGridViewLibros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewLibros.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodigoISBNDataGridViewTextBoxColumn, Me.TituloDataGridViewTextBoxColumn, Me.AutorDataGridViewTextBoxColumn, Me.StockDataGridViewTextBoxColumn})
        Me.DataGridViewLibros.DataSource = Me.LibrosBindingSource
        Me.DataGridViewLibros.Location = New System.Drawing.Point(16, 149)
        Me.DataGridViewLibros.Name = "DataGridViewLibros"
        Me.DataGridViewLibros.ReadOnly = True
        Me.DataGridViewLibros.RowHeadersWidth = 20
        Me.DataGridViewLibros.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewLibros.Size = New System.Drawing.Size(520, 144)
        Me.DataGridViewLibros.TabIndex = 4
        '
        'CodigoISBNDataGridViewTextBoxColumn
        '
        Me.CodigoISBNDataGridViewTextBoxColumn.DataPropertyName = "CodigoISBN"
        Me.CodigoISBNDataGridViewTextBoxColumn.HeaderText = "CodigoISBN"
        Me.CodigoISBNDataGridViewTextBoxColumn.Name = "CodigoISBNDataGridViewTextBoxColumn"
        Me.CodigoISBNDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TituloDataGridViewTextBoxColumn
        '
        Me.TituloDataGridViewTextBoxColumn.DataPropertyName = "Titulo"
        Me.TituloDataGridViewTextBoxColumn.HeaderText = "Titulo"
        Me.TituloDataGridViewTextBoxColumn.Name = "TituloDataGridViewTextBoxColumn"
        Me.TituloDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AutorDataGridViewTextBoxColumn
        '
        Me.AutorDataGridViewTextBoxColumn.DataPropertyName = "Autor"
        Me.AutorDataGridViewTextBoxColumn.HeaderText = "Autor"
        Me.AutorDataGridViewTextBoxColumn.Name = "AutorDataGridViewTextBoxColumn"
        Me.AutorDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StockDataGridViewTextBoxColumn
        '
        Me.StockDataGridViewTextBoxColumn.DataPropertyName = "Stock"
        Me.StockDataGridViewTextBoxColumn.HeaderText = "Stock"
        Me.StockDataGridViewTextBoxColumn.Name = "StockDataGridViewTextBoxColumn"
        Me.StockDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LibrosBindingSource
        '
        Me.LibrosBindingSource.DataMember = "Libros"
        Me.LibrosBindingSource.DataSource = Me.DBNETDataSet
        '
        'DBNETDataSet
        '
        Me.DBNETDataSet.DataSetName = "DBNETDataSet"
        Me.DBNETDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LibrosTableAdapter
        '
        Me.LibrosTableAdapter.ClearBeforeFill = True
        '
        'cancelar
        '
        Me.cancelar.Location = New System.Drawing.Point(380, 299)
        Me.cancelar.Name = "cancelar"
        Me.cancelar.Size = New System.Drawing.Size(75, 23)
        Me.cancelar.TabIndex = 5
        Me.cancelar.Text = "Cancelar"
        Me.cancelar.UseVisualStyleBackColor = True
        '
        'seleccionar
        '
        Me.seleccionar.Location = New System.Drawing.Point(461, 299)
        Me.seleccionar.Name = "seleccionar"
        Me.seleccionar.Size = New System.Drawing.Size(75, 23)
        Me.seleccionar.TabIndex = 6
        Me.seleccionar.Text = "Seleccionar"
        Me.seleccionar.UseVisualStyleBackColor = True
        '
        'BusquedaLibro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 438)
        Me.Controls.Add(Me.seleccionar)
        Me.Controls.Add(Me.cancelar)
        Me.Controls.Add(Me.DataGridViewLibros)
        Me.Controls.Add(Me.buscar_libro)
        Me.Controls.Add(Me.lb1)
        Me.Controls.Add(Me.titulo_libro)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Name = "BusquedaLibro"
        Me.Text = "BusquedaLibro"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.titulo_libro, 0)
        Me.Controls.SetChildIndex(Me.lb1, 0)
        Me.Controls.SetChildIndex(Me.buscar_libro, 0)
        Me.Controls.SetChildIndex(Me.DataGridViewLibros, 0)
        Me.Controls.SetChildIndex(Me.cancelar, 0)
        Me.Controls.SetChildIndex(Me.seleccionar, 0)
        CType(Me.DataGridViewLibros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LibrosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents titulo_libro As TextBox
    Friend WithEvents lb1 As Label
    Friend WithEvents buscar_libro As Button
    Friend WithEvents DataGridViewLibros As DataGridView
    Friend WithEvents DBNETDataSet As DBNETDataSet
    Friend WithEvents LibrosBindingSource As BindingSource
    Friend WithEvents LibrosTableAdapter As DBNETDataSetTableAdapters.LibrosTableAdapter
    Friend WithEvents CodigoISBNDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TituloDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AutorDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StockDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents cancelar As Button
    Friend WithEvents seleccionar As Button
End Class
