﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Libros
    Inherits Plantilla

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodigoISBNLabel As System.Windows.Forms.Label
        Dim TituloLabel As System.Windows.Forms.Label
        Dim AutorLabel As System.Windows.Forms.Label
        Dim StockLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Libros))
        Me.DBNETDataSet = New NuevaBaseDatos.DBNETDataSet()
        Me.LibrosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LibrosTableAdapter = New NuevaBaseDatos.DBNETDataSetTableAdapters.LibrosTableAdapter()
        Me.TableAdapterManager = New NuevaBaseDatos.DBNETDataSetTableAdapters.TableAdapterManager()
        Me.CodigoISBNTextBox = New System.Windows.Forms.TextBox()
        Me.TituloTextBox = New System.Windows.Forms.TextBox()
        Me.AutorTextBox = New System.Windows.Forms.TextBox()
        Me.StockTextBox = New System.Windows.Forms.TextBox()
        Me.Guardar = New System.Windows.Forms.Button()
        Me.Cancelar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CodigoISBNLabel = New System.Windows.Forms.Label()
        TituloLabel = New System.Windows.Forms.Label()
        AutorLabel = New System.Windows.Forms.Label()
        StockLabel = New System.Windows.Forms.Label()
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LibrosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Text = "Libros"
        '
        'CodigoISBNLabel
        '
        CodigoISBNLabel.AutoSize = True
        CodigoISBNLabel.Location = New System.Drawing.Point(40, 97)
        CodigoISBNLabel.Name = "CodigoISBNLabel"
        CodigoISBNLabel.Size = New System.Drawing.Size(71, 13)
        CodigoISBNLabel.TabIndex = 2
        CodigoISBNLabel.Text = "Codigo ISBN:"
        '
        'TituloLabel
        '
        TituloLabel.AutoSize = True
        TituloLabel.Location = New System.Drawing.Point(40, 123)
        TituloLabel.Name = "TituloLabel"
        TituloLabel.Size = New System.Drawing.Size(36, 13)
        TituloLabel.TabIndex = 4
        TituloLabel.Text = "Titulo:"
        '
        'AutorLabel
        '
        AutorLabel.AutoSize = True
        AutorLabel.Location = New System.Drawing.Point(40, 149)
        AutorLabel.Name = "AutorLabel"
        AutorLabel.Size = New System.Drawing.Size(35, 13)
        AutorLabel.TabIndex = 6
        AutorLabel.Text = "Autor:"
        '
        'StockLabel
        '
        StockLabel.AutoSize = True
        StockLabel.Location = New System.Drawing.Point(40, 175)
        StockLabel.Name = "StockLabel"
        StockLabel.Size = New System.Drawing.Size(38, 13)
        StockLabel.TabIndex = 8
        StockLabel.Text = "Stock:"
        '
        'DBNETDataSet
        '
        Me.DBNETDataSet.DataSetName = "DBNETDataSet"
        Me.DBNETDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LibrosBindingSource
        '
        Me.LibrosBindingSource.DataMember = "Libros"
        Me.LibrosBindingSource.DataSource = Me.DBNETDataSet
        '
        'LibrosTableAdapter
        '
        Me.LibrosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.LibrosTableAdapter = Me.LibrosTableAdapter
        Me.TableAdapterManager.PrestamosTableAdapter = Nothing
        Me.TableAdapterManager.SociosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = NuevaBaseDatos.DBNETDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsuariosTableAdapter = Nothing
        '
        'CodigoISBNTextBox
        '
        Me.CodigoISBNTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LibrosBindingSource, "CodigoISBN", True))
        Me.CodigoISBNTextBox.Location = New System.Drawing.Point(117, 94)
        Me.CodigoISBNTextBox.Name = "CodigoISBNTextBox"
        Me.CodigoISBNTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodigoISBNTextBox.TabIndex = 3
        '
        'TituloTextBox
        '
        Me.TituloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LibrosBindingSource, "Titulo", True))
        Me.TituloTextBox.Location = New System.Drawing.Point(117, 120)
        Me.TituloTextBox.Name = "TituloTextBox"
        Me.TituloTextBox.Size = New System.Drawing.Size(159, 20)
        Me.TituloTextBox.TabIndex = 5
        '
        'AutorTextBox
        '
        Me.AutorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LibrosBindingSource, "Autor", True))
        Me.AutorTextBox.Location = New System.Drawing.Point(117, 146)
        Me.AutorTextBox.Name = "AutorTextBox"
        Me.AutorTextBox.Size = New System.Drawing.Size(159, 20)
        Me.AutorTextBox.TabIndex = 7
        '
        'StockTextBox
        '
        Me.StockTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LibrosBindingSource, "Stock", True))
        Me.StockTextBox.Location = New System.Drawing.Point(117, 172)
        Me.StockTextBox.Name = "StockTextBox"
        Me.StockTextBox.Size = New System.Drawing.Size(100, 20)
        Me.StockTextBox.TabIndex = 9
        '
        'Guardar
        '
        Me.Guardar.Location = New System.Drawing.Point(224, 211)
        Me.Guardar.Name = "Guardar"
        Me.Guardar.Size = New System.Drawing.Size(75, 23)
        Me.Guardar.TabIndex = 10
        Me.Guardar.Text = "Guardar"
        Me.Guardar.UseVisualStyleBackColor = True
        '
        'Cancelar
        '
        Me.Cancelar.Location = New System.Drawing.Point(142, 211)
        Me.Cancelar.Name = "Cancelar"
        Me.Cancelar.Size = New System.Drawing.Size(75, 23)
        Me.Cancelar.TabIndex = 11
        Me.Cancelar.Text = "Cancelar"
        Me.Cancelar.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(305, 87)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(219, 147)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'Libros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 317)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Cancelar)
        Me.Controls.Add(Me.Guardar)
        Me.Controls.Add(CodigoISBNLabel)
        Me.Controls.Add(Me.CodigoISBNTextBox)
        Me.Controls.Add(TituloLabel)
        Me.Controls.Add(Me.TituloTextBox)
        Me.Controls.Add(AutorLabel)
        Me.Controls.Add(Me.AutorTextBox)
        Me.Controls.Add(StockLabel)
        Me.Controls.Add(Me.StockTextBox)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Name = "Libros"
        Me.Text = "Libros"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.StockTextBox, 0)
        Me.Controls.SetChildIndex(StockLabel, 0)
        Me.Controls.SetChildIndex(Me.AutorTextBox, 0)
        Me.Controls.SetChildIndex(AutorLabel, 0)
        Me.Controls.SetChildIndex(Me.TituloTextBox, 0)
        Me.Controls.SetChildIndex(TituloLabel, 0)
        Me.Controls.SetChildIndex(Me.CodigoISBNTextBox, 0)
        Me.Controls.SetChildIndex(CodigoISBNLabel, 0)
        Me.Controls.SetChildIndex(Me.Guardar, 0)
        Me.Controls.SetChildIndex(Me.Cancelar, 0)
        Me.Controls.SetChildIndex(Me.PictureBox1, 0)
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LibrosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DBNETDataSet As DBNETDataSet
    Friend WithEvents LibrosBindingSource As BindingSource
    Friend WithEvents LibrosTableAdapter As DBNETDataSetTableAdapters.LibrosTableAdapter
    Friend WithEvents TableAdapterManager As DBNETDataSetTableAdapters.TableAdapterManager
    Friend WithEvents CodigoISBNTextBox As TextBox
    Friend WithEvents TituloTextBox As TextBox
    Friend WithEvents AutorTextBox As TextBox
    Friend WithEvents StockTextBox As TextBox
    Friend WithEvents Guardar As Button
    Friend WithEvents Cancelar As Button
    Friend WithEvents PictureBox1 As PictureBox
End Class
