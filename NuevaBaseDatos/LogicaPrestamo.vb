﻿Public Class LogicaPrestamo

    Public Sub PrestarLibro(ByVal socio As Decimal, ByVal idsbn As String)
        'Verficiar Stock
        Dim AdaptadorLibros As New DBNETDataSetTableAdapters.LibrosTableAdapter
        Dim LibrosDS As New DBNETDataSet
        AdaptadorLibros.FillByLibro(LibrosDS.Libros, idsbn)
        If LibrosDS.Libros.Count = 0 Then
            Throw New ApplicationException("Error, No existe del libro deseado")
        End If
        If LibrosDS.Libros(0).Stock = 0 Then
            Throw New ApplicationException("Error, No hay disponibilidad del libro deseado")
        End If
        'Descontar Stock
        LibrosDS.Libros(0).Stock -= 1
        AdaptadorLibros.Update(LibrosDS.Libros)
        ' Prestar Libro
        Dim adaptadorPrestamos As New DBNETDataSetTableAdapters.PrestamosTableAdapter
        Dim PrestamosDS As New DBNETDataSet
        PrestamosDS.Prestamos.AddPrestamosRow(socio, idsbn, Now, False)
        adaptadorPrestamos.Update(PrestamosDS.Prestamos)
    End Sub

End Class
