﻿Public Class Socios


    Private Sub Socios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DBNETDataSet.Socios' Puede moverla o quitarla según sea necesario.
        Me.SociosTableAdapter.Fill(Me.DBNETDataSet.Socios)
        SociosBindingSource.AddNew()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Validate()
        Me.SociosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DBNETDataSet)
        System.Windows.Forms.MessageBox.Show(Me, "Grabacion
Satisfactoria.", "Guardar", System.Windows.Forms.MessageBoxButtons.OK,
       System.Windows.Forms.MessageBoxIcon.Information)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close()
    End Sub
End Class