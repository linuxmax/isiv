﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BusquedaSocio
    Inherits Plantilla

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nombre_socio = New System.Windows.Forms.TextBox()
        Me.buscar_socio = New System.Windows.Forms.Button()
        Me.DataGridViewSocios = New System.Windows.Forms.DataGridView()
        Me.DNIDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApellidoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TelefonoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmailDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SociosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DBNETDataSet = New NuevaBaseDatos.DBNETDataSet()
        Me.SociosTableAdapter = New NuevaBaseDatos.DBNETDataSetTableAdapters.SociosTableAdapter()
        Me.seleccionar = New System.Windows.Forms.Button()
        Me.cancelar = New System.Windows.Forms.Button()
        CType(Me.DataGridViewSocios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SociosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Text = "Buscar Socios"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre"
        '
        'nombre_socio
        '
        Me.nombre_socio.Location = New System.Drawing.Point(69, 93)
        Me.nombre_socio.Name = "nombre_socio"
        Me.nombre_socio.Size = New System.Drawing.Size(146, 20)
        Me.nombre_socio.TabIndex = 2
        '
        'buscar_socio
        '
        Me.buscar_socio.Location = New System.Drawing.Point(221, 90)
        Me.buscar_socio.Name = "buscar_socio"
        Me.buscar_socio.Size = New System.Drawing.Size(75, 23)
        Me.buscar_socio.TabIndex = 3
        Me.buscar_socio.Text = "Buscar"
        Me.buscar_socio.UseVisualStyleBackColor = True
        '
        'DataGridViewSocios
        '
        Me.DataGridViewSocios.AllowUserToAddRows = False
        Me.DataGridViewSocios.AllowUserToDeleteRows = False
        Me.DataGridViewSocios.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewSocios.AutoGenerateColumns = False
        Me.DataGridViewSocios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewSocios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DNIDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.ApellidoDataGridViewTextBoxColumn, Me.TelefonoDataGridViewTextBoxColumn, Me.EmailDataGridViewTextBoxColumn})
        Me.DataGridViewSocios.DataSource = Me.SociosBindingSource
        Me.DataGridViewSocios.Location = New System.Drawing.Point(15, 119)
        Me.DataGridViewSocios.Name = "DataGridViewSocios"
        Me.DataGridViewSocios.ReadOnly = True
        Me.DataGridViewSocios.RowHeadersWidth = 20
        Me.DataGridViewSocios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewSocios.Size = New System.Drawing.Size(534, 194)
        Me.DataGridViewSocios.TabIndex = 4
        '
        'DNIDataGridViewTextBoxColumn
        '
        Me.DNIDataGridViewTextBoxColumn.DataPropertyName = "DNI"
        Me.DNIDataGridViewTextBoxColumn.HeaderText = "DNI"
        Me.DNIDataGridViewTextBoxColumn.Name = "DNIDataGridViewTextBoxColumn"
        Me.DNIDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ApellidoDataGridViewTextBoxColumn
        '
        Me.ApellidoDataGridViewTextBoxColumn.DataPropertyName = "Apellido"
        Me.ApellidoDataGridViewTextBoxColumn.HeaderText = "Apellido"
        Me.ApellidoDataGridViewTextBoxColumn.Name = "ApellidoDataGridViewTextBoxColumn"
        Me.ApellidoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TelefonoDataGridViewTextBoxColumn
        '
        Me.TelefonoDataGridViewTextBoxColumn.DataPropertyName = "Telefono"
        Me.TelefonoDataGridViewTextBoxColumn.HeaderText = "Telefono"
        Me.TelefonoDataGridViewTextBoxColumn.Name = "TelefonoDataGridViewTextBoxColumn"
        Me.TelefonoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EmailDataGridViewTextBoxColumn
        '
        Me.EmailDataGridViewTextBoxColumn.DataPropertyName = "Email"
        Me.EmailDataGridViewTextBoxColumn.HeaderText = "Email"
        Me.EmailDataGridViewTextBoxColumn.Name = "EmailDataGridViewTextBoxColumn"
        Me.EmailDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SociosBindingSource
        '
        Me.SociosBindingSource.DataMember = "Socios"
        Me.SociosBindingSource.DataSource = Me.DBNETDataSet
        '
        'DBNETDataSet
        '
        Me.DBNETDataSet.DataSetName = "DBNETDataSet"
        Me.DBNETDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SociosTableAdapter
        '
        Me.SociosTableAdapter.ClearBeforeFill = True
        '
        'seleccionar
        '
        Me.seleccionar.Location = New System.Drawing.Point(474, 319)
        Me.seleccionar.Name = "seleccionar"
        Me.seleccionar.Size = New System.Drawing.Size(75, 23)
        Me.seleccionar.TabIndex = 5
        Me.seleccionar.Text = "Seleccionar"
        Me.seleccionar.UseVisualStyleBackColor = True
        '
        'cancelar
        '
        Me.cancelar.Location = New System.Drawing.Point(393, 319)
        Me.cancelar.Name = "cancelar"
        Me.cancelar.Size = New System.Drawing.Size(75, 23)
        Me.cancelar.TabIndex = 6
        Me.cancelar.Text = "Cancelar"
        Me.cancelar.UseVisualStyleBackColor = True
        '
        'BusquedaSocio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 438)
        Me.Controls.Add(Me.cancelar)
        Me.Controls.Add(Me.seleccionar)
        Me.Controls.Add(Me.DataGridViewSocios)
        Me.Controls.Add(Me.buscar_socio)
        Me.Controls.Add(Me.nombre_socio)
        Me.Controls.Add(Me.Label2)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Name = "BusquedaSocio"
        Me.Text = "BusquedaSocio"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.nombre_socio, 0)
        Me.Controls.SetChildIndex(Me.buscar_socio, 0)
        Me.Controls.SetChildIndex(Me.DataGridViewSocios, 0)
        Me.Controls.SetChildIndex(Me.seleccionar, 0)
        Me.Controls.SetChildIndex(Me.cancelar, 0)
        CType(Me.DataGridViewSocios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SociosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DBNETDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents nombre_socio As TextBox
    Friend WithEvents buscar_socio As Button
    Friend WithEvents DataGridViewSocios As DataGridView
    Friend WithEvents DBNETDataSet As DBNETDataSet
    Friend WithEvents SociosBindingSource As BindingSource
    Friend WithEvents SociosTableAdapter As DBNETDataSetTableAdapters.SociosTableAdapter
    Friend WithEvents DNIDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ApellidoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TelefonoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EmailDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents seleccionar As Button
    Friend WithEvents cancelar As Button
End Class
