﻿Public Class Prestamos

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close()
    End Sub


    Private Sub busca_socio_Click(sender As Object, e As EventArgs) Handles busca_socio.Click
        Dim BusquedaSocios As New BusquedaSocio
        BusquedaSocios.WindowState = FormWindowState.Normal
        BusquedaSocios.ShowDialog()
        cod_socio.Text = BusquedaSocios.DNI
        socio.Text = BusquedaSocios.Apellido
    End Sub

    Private Sub busca_libro_Click(sender As Object, e As EventArgs) Handles busca_libro.Click
        Dim BusquedaLibros As New BusquedaLibro
        BusquedaLibros.WindowState = FormWindowState.Normal
        BusquedaLibros.ShowDialog()
        cod_libro.Text = BusquedaLibros.IDSBN
        libro.Text = BusquedaLibros.Titulo
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim LogicaPrestamosBN As New LogicaPrestamo
        LogicaPrestamosBN.PrestarLibro(cod_socio.Text, cod_libro.Text)
        Close()

    End Sub
End Class